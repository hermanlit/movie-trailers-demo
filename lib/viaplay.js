'use strict';

const Request = require('request-promise');

module.exports = {
  baseUri: 'https://content.viaplay.se/pcdash-se/film',

  all: function() {
    return Request({ method: 'GET', uri: this.baseUri, json: true })
      .then((data) => {
        let topMoviesData = data['_embedded']['viaplay:blocks'][0]['_embedded']['viaplay:products'];
        return topMoviesData.map(movie => { return {
          title: movie['content']['title'],
          publicPath: movie['publicPath'],
          imageUrl: movie['content']['images']['boxart']['url']
        }});
      });
  },

  one: function(title) {
    let movieUri = this.baseUri + '/' + title;

    return Request({ method: 'GET', uri: movieUri, json: true })
      .then((data) => {
        let movieData = data['_embedded']['viaplay:blocks'][0]['_embedded']['viaplay:product'];
        return parseMovieData(movieData);
      });
  }
}

function parseMovieData(movieJson) {
  return {
    title: movieJson['content']['title'],
    publicPath: movieJson['publicPath'],
    images: {
      boxart: movieJson['content']['images']['boxart']['url'],
      landscape: movieJson['content']['images']['landscape']['url']
    },
    actors: movieJson['content']['people']['actors'],
    directors: movieJson['content']['people']['directors'],
    country: movieJson['content']['production']['country'],
    imdbId: movieJson['content']['imdb']['id'],
    synopsis: movieJson['content']['synopsis']
  }
}
