'use strict';

const Request = require('request-promise');

module.exports = {
  trailerFor: function(movieTitle) {
    let encodedTitle = encodeURIComponent(movieTitle);

    return Request({
      method: 'GET',
      uri: `http://trailersapi.com/trailers.json?movie=${encodedTitle}&limit=1&width=320`
    })
    .then(body => {
      let trailers = JSON.parse(body);

      if (trailers.length > 0) {
        return trailers[0]['code'];
      } else {
        return null;
      }
    })
  }
};
