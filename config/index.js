'use strict';

module.exports = {
  connection: {
    host: '0.0.0.0',
    port: process.env.PORT || 8080
  }
};
