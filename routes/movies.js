'use strict';

const viaplay = require('../lib/viaplay');

module.exports = {
  method: 'GET',
  path: '/movies',
  handler: (request, reply) => {
    viaplay.all()
      .then(movies => reply.view('movies', { movies: movies }))
      .catch(err => reply(Boom.badImplementation(err)));
  },
  config: {
    cache: {
      expiresIn: 300 * 1000,
      privacy: 'private'
    }
  }
};
