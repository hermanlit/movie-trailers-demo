'use strict';

const Promise = require('bluebird');
const Boom = require('boom');
const viaplay = require('../lib/viaplay');
const trailers = require('../lib/trailers');

module.exports = {
  method: 'GET',
  path: '/movies/{title}',
  handler: (request, reply) => {
    let movieTitle = request.params.title;

    viaplay.one(movieTitle).then((movie) => {
      return trailers.trailerFor(movie.title).then(trailerIframe => {
        reply.view('movie', { movie: movie, trailer: trailerIframe });
      });
    })
    .catch(err => reply(Boom.badImplementation(err)));
  },
  config: {
    cache: {
      expiresIn: 300 * 1000,
      privacy: 'private'
    }
  }
};
