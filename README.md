## Movie Trailers Demo

Demo uses some of the Viaplay API endpoint and simple API from TrailersAPI to resolve movie trailers;

Demo consists of two `pages`:

 - `All movies`: simple fetches movies from Viaplay in top list category;
 - `Movie details`: short description and trailer (if there is one) or poster image (if not);

### Installation & Run

- Clone this repo
- Run `npm i && npm start`
- Visit `http://localhost:8080/movies`

### Tools used
Didn't use any SPA frameworks here since it would be overhead for two pages;

- `Hapi` framework for main functionality
- `Handlebars` for templating
- `Bootstrap` for grid

### Things to add

- Some SPA framework in case there will be more functionality;
- Proper error handling;
- Proper Logging;
