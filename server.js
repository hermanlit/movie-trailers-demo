'use strict';

const Hapi = require('hapi');
const Boom = require('boom');
const Inert = require('inert');
const Vision = require('vision');
const Request = require('request-promise');
const glob = require('glob');
const path = require('path')

const config = require('./config');
const viaplay = require('./lib/viaplay');
const trailers = require('./lib/trailers');

const server = new Hapi.Server();

server.connection(config.connection);

server.register([
  require('inert'),
  require('vision')
], (err) => {
  if (err) throw err;

  server.views({
    engines: {
      html: require('handlebars')
    },
    relativeTo: __dirname,
    path: 'views',
    layoutPath: 'views',
    layout: 'layout'
  });

  glob.sync('routes/*.js', { root: __dirname }).forEach((file) => {
    let route = require(path.join(__dirname, file));
    server.route(route);
  });

  // Static files handler
  server.route({
    method: 'GET',
    path: '/{param*}',
    handler: {
      directory: {
        path: 'public'
      }
    }
  });
});

server.start((err) => {
  if (err) throw err;
  console.log(`Server listening at ${server.info.uri}`);
})
